{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module CsSyd.Facturator.Types
  ( module CsSyd.Facturator.Types
  , Price(..)
  ) where

import Data.Aeson
import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.Time as Time
import Data.Time.Calendar
import Data.Validity
import Data.Validity.Text ()
import Data.Validity.Time ()
import GHC.Generics (Generic)
import Text.Pandoc

import CsSyd.Docter

data Invoice =
  Invoice
    { invoiceFrom :: InvoiceFrom
    , invoiceTo :: InvoiceTo
    , invoiceDate :: Day
    , invoiceDueDate :: Maybe Day
    , invoiceReference :: Maybe Text
    , invoiceInvoiceNumber :: Int
    , invoiceLines :: [InvoiceLine]
    }
  deriving (Show, Eq, Generic)

instance Validity Invoice

instance FromJSON Invoice where
  parseJSON (Object o) =
    Invoice <$> o .: "from" <*> o .: "to" <*> o .: "date" <*> o .: "due date" <*>
    o .: "reference" <*>
    o .: "invoice-number" <*>
    o .: "lines"
  parseJSON _ = mempty

instance ToJSON Invoice where
  toJSON Invoice {..} =
    object
      [ "from" .= invoiceFrom
      , "to" .= invoiceTo
      , "date" .= invoiceDate
      , "due date" .= invoiceDueDate
      , "reference" .= invoiceReference
      , "invoice-number" .= invoiceInvoiceNumber
      , "lines" .= invoiceLines
      ]

instance ToJSON (ForMustache Invoice) where
  toJSON (ForMustache Invoice {..}) =
    let InvoiceFrom {..} = invoiceFrom
        InvoiceTo {..} = invoiceTo
     in object
          [ "from" .= ForMustache invoiceFrom
          , "to" .= ForMustache invoiceTo
          , "date" .= ForMustache invoiceDate
          , "due-date" .= ForMustache invoiceDueDate
          , "reference" .= ForMustache invoiceReference
          , "lines" .= map ForMustache invoiceLines
          , "invoice-number" .= invoiceInvoiceNumber
          , "total-amount" .=
            ForMustache (priceAdds $ map lineAmount invoiceLines)
          ]

emptyInvoice :: Invoice
emptyInvoice =
  Invoice
    { invoiceFrom = emptyInvoiceFrom
    , invoiceTo = emptyInvoiceTo
    , invoiceDate = Time.fromGregorian 1970 1 1
    , invoiceDueDate = Nothing
    , invoiceReference = Nothing
    , invoiceInvoiceNumber = 0
    , invoiceLines = []
    }

data InvoiceFrom =
  InvoiceFrom
    { fromAddress :: Address
    , fromPhone :: PhoneNumber
    , fromEmail :: EmailAddress
    , fromPaymentDetails :: PaymentDetails
    , fromUid :: Text
    , fromVatNr :: Maybe Text
    }
  deriving (Show, Eq, Generic)

instance Validity InvoiceFrom

instance FromJSON InvoiceFrom where
  parseJSON =
    withObject "InvoiceFrom" $ \o ->
      InvoiceFrom <$> o .: "address" <*> o .: "phone" <*> o .: "email" <*>
      o .: "payment details" <*>
      o .:? "uid" .!= "" <*>
      o .:? "vat-nr"

instance ToJSON InvoiceFrom where
  toJSON InvoiceFrom {..} =
    object
      [ "address" .= fromAddress
      , "phone" .= fromPhone
      , "email" .= fromEmail
      , "payment details" .= fromPaymentDetails
      , "uid" .= fromUid
      , "vat-nr" .= fromVatNr
      ]

instance ToJSON (ForMustache InvoiceFrom) where
  toJSON (ForMustache InvoiceFrom {..}) =
    object
      [ "address" .= ForMustache fromAddress
      , "phone" .= ForMustache fromPhone
      , "email" .= fromEmail
      , "payment-details" .= ForMustache fromPaymentDetails
      , "uid" .= fromUid
      , "vat-nr" .= fromVatNr
      ]

emptyInvoiceFrom :: InvoiceFrom
emptyInvoiceFrom =
  InvoiceFrom
    { fromAddress = emptyAddress
    , fromPhone = emptyPhoneNumber
    , fromEmail = EmailAddress ""
    , fromPaymentDetails =
        PaymentDetails
          { paymentBeneficiary = ""
          , paymentIban = Nothing
          , paymentBic = Nothing
          , paymentExtra = Nothing
          }
    , fromUid = ""
    , fromVatNr = Nothing
    }

data InvoiceTo =
  InvoiceTo
    { toAddress :: Address
    , toVATNumber :: Maybe Text
    }
  deriving (Show, Eq, Generic)

instance Validity InvoiceTo

instance FromJSON InvoiceTo where
  parseJSON (Object o) = InvoiceTo <$> o .: "address" <*> o .:? "vat-nr"
  parseJSON _ = mempty

instance ToJSON InvoiceTo where
  toJSON InvoiceTo {..} =
    object ["address" .= toAddress, "vat-nr" .= toVATNumber]

instance ToJSON (ForMustache InvoiceTo) where
  toJSON (ForMustache InvoiceTo {..}) =
    object
      [ "address" .= ForMustache toAddress
      , "vat-nr" .=
        case toVATNumber of
          Nothing -> ""
          Just vn -> "VAT: " <> vn
      ]

emptyInvoiceTo :: InvoiceTo
emptyInvoiceTo = InvoiceTo {toAddress = emptyAddress, toVATNumber = Nothing}

data InvoiceLine =
  InvoiceLine
    { lineDescription :: Text
    , lineQuantity :: Double
    , linePrice :: Price
    }
  deriving (Show, Eq, Generic)

instance Validity InvoiceLine where
  validate il@InvoiceLine {..} =
    mconcat
      [ genericValidate il
      , validateNotNaN lineQuantity
      , validateNotInfinite lineQuantity
      ]

instance FromJSON InvoiceLine where
  parseJSON (Object o) =
    InvoiceLine <$> o .: "description" <*> o .: "quantity" <*> o .: "unit price"
  parseJSON _ = mempty

instance ToJSON InvoiceLine where
  toJSON InvoiceLine {..} =
    object
      [ "description" .= lineDescription
      , "quantity" .= lineQuantity
      , "unit price" .= linePrice
      ]

instance ToJSON (ForMustache InvoiceLine) where
  toJSON (ForMustache l@InvoiceLine {..}) =
    object
      [ "description" .= markdownFor lineDescription
      , "quantity" .= lineQuantity
      , "unit-price" .= ForMustache linePrice
      , "amount" .= ForMustache (lineAmount l)
      ]

lineAmount :: InvoiceLine -> Price
lineAmount InvoiceLine {..} = priceMult linePrice lineQuantity

markdownFor :: Text -> Value
markdownFor t =
  object
    [ "raw" .= t
    , "rendered" .=
      (either (T.pack . show) id $
       runPure $
       ((writeLaTeX def =<< (readMarkdown def t :: PandocPure Pandoc)) :: PandocPure Text))
    ]
