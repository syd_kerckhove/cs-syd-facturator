module CsSyd.Facturator.Generate.Types where

import Data.Text
import qualified Data.Time as Time

import CsSyd.Docter

import CsSyd.Facturator.Types

data GenerationAssignment =
  GenerationAssignment
    { genOutputDirective :: OutputDirective
    , genInvoiceFrom :: InvoiceFrom
    , genInvoiceTo :: InvoiceTo
    , genReference :: Maybe Text
    , genDate :: Time.Day
    , genDueDate :: Maybe Time.Day
    , genLines :: [InvoiceLine]
    }
  deriving (Show, Eq)
