{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}

module CsSyd.Facturator.Generate where

import Control.Applicative
import Data.Aeson
import qualified Data.ByteString.Char8 as SB8
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Time as Time
import qualified Data.Yaml as Yaml
import Path
import Path.IO
import System.Exit

import CsSyd.Docter

import CsSyd.Facturator.Generate.Types
import CsSyd.Facturator.OptParse.Types
import CsSyd.Facturator.Types

generate :: GenerationSettings -> IO ()
generate sets = do
  GenerationAssignment {..} <- assignGeneration sets
  let invoice =
        emptyInvoice
          { invoiceFrom = genInvoiceFrom
          , invoiceTo = genInvoiceTo
          , invoiceReference = genReference
          , invoiceDate = genDate
          , invoiceDueDate = genDueDate
          , invoiceLines = genLines
          }
  case genOutputDirective of
    ToStdOut -> SB8.putStrLn $ Yaml.encode invoice
    ToFile f -> writeJSON f invoice

assignGeneration :: GenerationSettings -> IO GenerationAssignment
assignGeneration GenerationSettings {..} =
  GenerationAssignment outputd <$> getfrom <*> getto <*>
  pure generateSetsReference <*>
  getdate <*>
  getduedate <*>
  getlines
  where
    outputd :: OutputDirective
    outputd =
      case generateSetsOutput of
        Nothing -> ToStdOut
        Just f -> ToFile f
    getfrom :: IO InvoiceFrom
    getfrom =
      getJSONOrYAMLWith
        generateSetsFrom
        emptyInvoiceFrom
        ($(mkRelDir "from/") </>)
    getto :: IO InvoiceTo
    getto =
      getJSONOrYAMLWith generateSetsTo emptyInvoiceTo ($(mkRelDir "to/") </>)
    getJSONOrYAMLWith ::
         FromJSON a
      => Maybe (Path Rel File)
      -> a
      -> (Path Rel File -> Path Rel File)
      -> IO a
    getJSONOrYAMLWith mf def_ func =
      case mf of
        Nothing -> pure def_
        Just tof -> do
          jpn <- setFileExtension ".json" $ func tof
          ypn <- setFileExtension ".yaml" $ func tof
          here <- getCurrentDir
          mjaf <- searchFile here jpn
          myaf <- searchFile here ypn
          case mjaf <|> myaf of
            Nothing ->
              die $
              unlines
                [ "Files"
                , toFilePath jpn
                , "and"
                , toFilePath ypn
                , "not found anywhere under"
                , toFilePath here
                ]
            Just af -> readJSONOrYaml af
    getdate :: IO Time.Day
    getdate =
      case generateSetsDay of
        Nothing -> do
          now <- Time.getZonedTime
          let nowutc = Time.zonedTimeToUTC now
          pure $ Time.utctDay nowutc
        Just d -> pure d
    getduedate =
      case generateSetsDueDays of
        Nothing -> pure Nothing
        Just i -> (Just . Time.addDays i) <$> getdate
    getlines =
      pure $
      generateSetsLines ++
      case generateSetsPreset of
        EmptyLines -> []
        WaitingTime mt mp mps ->
          [ InvoiceLine
              { lineDescription = "Waiting time"
              , lineQuantity = fromMaybe (read "NaN") mt
              , linePrice =
                  Price
                    { priceQuantity = fromMaybe (read "NaN") mp
                    , priceSign = maybe "" T.pack mps
                    }
              }
          ]
