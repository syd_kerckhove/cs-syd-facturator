{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CsSyd.Facturator.Automate where

import Control.Monad
import Path
import Path.IO
import Text.Mustache

import System.Exit

import CsSyd.Docter

import CsSyd.Facturator.Types

automate :: IO ()
automate = do
  here <- getCurrentDir
  let rawDir = here </> rawDataDir
      autoDir = here </> autoGenDir
      manualDir = here </> manualDataDir
  fs <- snd <$> listDirRecur rawDir
  template <-
    do errOrTempl <-
         automaticCompile
           [toFilePath manualDir]
           (toFilePath $ manualDir </> mainTemplateFile)
       case errOrTempl of
         Left err ->
           die $ unwords ["Failed to compile invoice template", show err]
         Right tmpl -> pure tmpl
  forM_ (filter (not . hidden) fs) $ \f -> do
    i <- readJSONOrYaml f
    case stripProperPrefix rawDir f of
      Nothing -> pure ()
      Just rf -> genPdfFor template (autoDir </> rf) (i :: Invoice)

rawDataDir :: Path Rel Dir
rawDataDir = $(mkRelDir "raw")

autoGenDir :: Path Rel Dir
autoGenDir = $(mkRelDir "auto")

manualDataDir :: Path Rel Dir
manualDataDir = $(mkRelDir "manual")

mainTemplateFile :: Path Rel File
mainTemplateFile = $(mkRelFile "invoice.tex")
