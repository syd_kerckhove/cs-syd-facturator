{-# OPTIONS_GHC -Wno-orphans #-}

module CsSyd.Facturator.Gen where

import TestImport

import CsSyd.Facturator.Types

import CsSyd.Docter.Gen ()

instance GenValid Invoice where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid InvoiceFrom where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid InvoiceTo where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid InvoiceLine where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
