{-# LANGUAGE TypeApplications #-}

module CsSyd.FacturatorSpec
  ( spec
  ) where

import Test.Hspec
import Test.Validity
import Test.Validity.Aeson

import CsSyd.Facturator.Types

import CsSyd.Docter.Gen ()
import CsSyd.Facturator.Gen ()

spec :: Spec
spec = do
  genValidSpec @Invoice
  jsonSpecOnValid @Invoice
  genValidSpec @InvoiceFrom
  jsonSpecOnValid @InvoiceFrom
  genValidSpec @InvoiceTo
  jsonSpecOnValid @InvoiceTo
  genValidSpec @InvoiceLine
  jsonSpecOnValid @InvoiceLine
