final: previous:
with final.haskell.lib;

let
  facturator =
    doBenchmark (
      failOnAllWarnings (
        disableLibraryProfiling (
          final.haskellPackages.callCabal2nix "facturator" ( final.gitignoreSource ( ../. ) ) {}
        )
      )
    );
in {
  inherit facturator;
  haskellPackages =
    previous.haskellPackages.override (
      old:
        {
          overrides =
            final.lib.composeExtensions (
              old.overrides or (_:
            _:
              {})
            ) (
              self: super:
                let
                  docterRepo =
                    final.fetchgit {
                      url =
                        "https://bitbucket.org/syd_kerckhove/cs-syd-docter";
                      rev = "c7a576194d5b54641ae6b4e4de8da392718c4867";
                      sha256 =
                        "sha256:1szk3bcv0k0qlgbcqkqr53ga3cvm4fkv95mjjig4i992grrzw97k";
                    };

                  docterPkg =
                    name:
                      disableLibraryProfiling (
                        self.callCabal2nix name ( docterRepo + "/${name}" ) {}
                      );
                  docterPackages =
                    final.lib.genAttrs [
                      "docter"
                      "docter-data-gen"
                    ] docterPkg;
                in
                  docterPackages // {
                  "facturator" = facturator;
              }
            );
        }
    );
}
